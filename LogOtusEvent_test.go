package LogOtusEvent

import (
	"bytes"
	"testing"
	"time"
)

func TestLogOtusEvent(t *testing.T) {
	var b bytes.Buffer

	tests := []struct {
		name   string
		writer bytes.Buffer
		event  OtusEvent
		want   string
	}{
		{
			name:   "submitted",
			writer: b,
			event:  HwSubmitted{3456, "code", "please take a look at my homework"},
			want:   time.Now().Format("2006-01-02") + ` submitted 3456 "please take a look at my homework"`,
		},
		{
			name:   "accepted",
			writer: b,
			event:  HwAccepted{3456, 4},
			want:   time.Now().Format("2006-01-02") + ` accepted 3456 4`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			LogOtusEvent(tt.event, &tt.writer)
			got := tt.writer.String()
			if tt.want != got {
				t.Errorf("wanted %s not equal %s\n", tt.want, got)
			}
		})

	}

}

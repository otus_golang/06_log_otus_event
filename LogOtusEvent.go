package LogOtusEvent

import (
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

type HwAccepted struct {
	Id int
	Grade int
}

type HwSubmitted struct {
	Id int
	Code string
	Comment string
}

func (e HwAccepted) String() string {
	s := []string{
		time.Now().Format("2006-01-02"),
		"accepted",
		strconv.Itoa(e.Id),
		strconv.Itoa(e.Grade),
	}
	return strings.Join(s, " ")
}

func (e HwSubmitted) String() string {
	s := []string{
		time.Now().Format("2006-01-02"),
		"submitted",
		strconv.Itoa(e.Id),
		`"` +e.Comment + `"`,
	}
	return strings.Join(s, " ")
}


type OtusEvent interface {
	fmt.Stringer
}

func LogOtusEvent(e OtusEvent, w io.Writer) {
	w.Write([]byte(e.String()))
}